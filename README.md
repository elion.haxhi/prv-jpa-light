# PranveraApp JPA Light

Description:
This is a java se application meant to help validating the domain model
during the design phase when we have a good picture of the client requirements.
You can connect with different database like mysql ,oracle,postgres etc.
How to work with this app.
1. Create your entities and put then in the prv.jpa.light.entity package
2. In the META_INF folder create your persistence.xml file
3. In main package create youe main class connect with the db using entitymanagerfactory
   and then retrieve your domain from the database.
This is good because you have a clear view of what is going on with the data.

NB: You can aggregate this app to real webapplication just copy and paste the
the META-INF folder (it is recomended to copy that inside a test folder).

This app is on cloud on gitlab.

This project should be a helper during the the creation of the business logic with TDD


