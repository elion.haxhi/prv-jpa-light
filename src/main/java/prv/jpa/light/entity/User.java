package prv.jpa.light.entity;

import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "prv_user")
@Getter
@Setter
@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(doNotUseGetters = true, exclude = "authorities")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(nullable = false)
    private String username;

    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @LazyCollection(LazyCollectionOption.TRUE)
    @JoinTable(name="prv_user_authority",
            joinColumns={@JoinColumn(name="prv_user_id", referencedColumnName="id")},
            inverseJoinColumns={@JoinColumn(name="prv_authority_id", referencedColumnName="id")}
    )
    private List<Authority> authorities = new ArrayList<>();

    public String getPassword() {
        return password;
    }
}
