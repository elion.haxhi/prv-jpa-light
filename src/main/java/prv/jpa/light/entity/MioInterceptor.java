package prv.jpa.light.entity;

import lombok.*;

import javax.persistence.*;

import java.util.Date;

import static prv.jpa.light.entity.MioInterceptor.INTERCEPTOR;

@Entity
@Table(name = INTERCEPTOR)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class MioInterceptor {
    public static final String INTERCEPTOR = "prv_interceptor";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(columnDefinition="TIMESTAMP(6)")
    @Temporal(TemporalType.DATE)
    private Date callDate;
    private String srvName;
    private String ip;
}
