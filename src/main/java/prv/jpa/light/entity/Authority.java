package prv.jpa.light.entity;

import lombok.*;

import javax.persistence.*;

import static prv.jpa.light.entity.Authority.AUTHORITY;

@Entity
@Table(name = AUTHORITY)
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode
@ToString
public class Authority {
    public static final String AUTHORITY = "prv_authority";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String authority;


}
